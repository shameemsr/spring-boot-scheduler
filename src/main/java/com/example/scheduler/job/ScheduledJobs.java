package com.example.scheduler.job;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledJobs {

	private static final Logger log = LoggerFactory.getLogger(ScheduledJobs.class);
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("kk:mm:ss.SSS");

	@Scheduled(fixedRateString = "${fixed.rate.job1}")
	void executeJob1() {
		log.info("Job 1 - Trigger Time: " + sdf.format(new Date()));
	}
	
	@Scheduled(fixedRateString = "${fixed.rate.job2}")
	void executeJob2() {
		log.info("Job 2 - Trigger Time: " + sdf.format(new Date()));
	}
}
