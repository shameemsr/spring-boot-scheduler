### About

This sample **Spring Boot** project demonstrates *scheduling* of tasks by using `@EnableScheduling` and `@Scheduled` annotations. 

---

### Code Example

`@EnableScheduling` is used on a `Configuration` class rather than on the main application class. This enables us to to *enable/disable scheduling* from a property value (`scheduling.enabled` in our case).  


**Config class** (`com.example.scheduler.config.SchedulingConfiguration`): enables scheduling based on the property `scheduling.enabled`.

```java
@Configuration
@EnableScheduling
@ConditionalOnProperty(name = "scheduling.enabled", matchIfMissing = true)
public class SchedulingConfiguration {

}
```

---

**Scheduled tasks** (`com.example.scheduler.job.ScheduledJobs`): tasks that are scheduled on a fixed rate basis. The fixed rate value is specified in `fixed.rate.job1` as `java.time.Duration` format. For example, `PT30S` is *30 seconds*.

```java
@Scheduled(fixedRateString = "${fixed.rate.job1}")
void executeJob1() {
	log.info("Job 1 - Trigger Time: " + sdf.format(new Date()));
}
```

---

** Property file** (`application.yml`)

```yml
scheduling:
  enabled: true

fixed:
  rate:
    job1: PT30S
    job2: PT1M10S

spring:
  task:
    scheduling:
      pool.size: 10
```

---

** Execution log**: an example execution log which shows that **Job 1** is triggered *once in every 30 seconds* and **Job 2** is triggered *once in every 1 minute and 10 seconds*. 

```
2019-10-26 08:35:20.724  INFO 13348 --- [   scheduling-3] com.example.scheduler.job.ScheduledJobs  : Job 1 - Trigger Time: 08:35:20.724
2019-10-26 08:35:30.722  INFO 13348 --- [   scheduling-1] com.example.scheduler.job.ScheduledJobs  : Job 2 - Trigger Time: 08:35:30.722
2019-10-26 08:35:50.723  INFO 13348 --- [   scheduling-4] com.example.scheduler.job.ScheduledJobs  : Job 1 - Trigger Time: 08:35:50.722
2019-10-26 08:36:20.722  INFO 13348 --- [   scheduling-5] com.example.scheduler.job.ScheduledJobs  : Job 1 - Trigger Time: 08:36:20.722
2019-10-26 08:36:40.720  INFO 13348 --- [   scheduling-2] com.example.scheduler.job.ScheduledJobs  : Job 2 - Trigger Time: 08:36:40.720
2019-10-26 08:36:50.725  INFO 13348 --- [   scheduling-3] com.example.scheduler.job.ScheduledJobs  : Job 1 - Trigger Time: 08:36:50.724
```

---



### Reference Documentation
For further reference, please consider the following sections:


* [Spring Task Execution and Scheduling](https://docs.spring.io/spring/docs/3.2.x/spring-framework-reference/html/scheduling.html)

* [Spring Boot Scheduling Tasks Sample Project](https://spring.io/guides/gs/scheduling-tasks/)

